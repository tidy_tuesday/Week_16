
library(tidyverse)
library(readxl)
library(wordcloud)
library(wesanderson)

my_theme <- theme(text = element_text(family = "Comfortaa", color = "#22211d"),
                  rect = element_rect(fill = "#f5f5f2", color = NA), 
                  plot.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.background = element_rect(fill = "#f5f5f2", color = NA), 
                  legend.background = element_rect(fill = "#f5f5f2", color = NA),
                  legend.key = element_rect(fill = "#f5f5f2"),
                  plot.caption = element_text(size = 6))


path = "week16_exercise.xlsx"                       # path to excel file
sheets <- excel_sheets(path = path)                 # get sheet names

df_source <- read_xlsx(path, sheet=sheets[1])       ## import data from each
df_tidy  <- read_xlsx(path, sheet=sheets[2])        ## sheet, individually

# view unique values of sex and work_status
unique(df_tidy$sex)
unique(df_tidy$work_status)

df_tidy$exercise <- as.double(df_tidy$exercise)

# assign avg to var for easier calculations -- this could have been done much better...
both_total_avg <- df_tidy %>%
  filter(sex == "both", work_status=="total", state == "state_average") %>%
  select(exercise)

# construct bar chart
df_tidy %>%
  filter(sex == "both", work_status=="total") %>%
  mutate(difference = exercise - both_total_avg$exercise,
         col = case_when(difference < 0 ~ "#C27D38", 
                         difference > 0 ~ "#798E87", 
                         TRUE ~ "#CCC591"),
         nudge = case_when(difference < 0 ~ -0.3, 
                           difference > 0 ~ 0.3, 
                           TRUE ~ 0)) %>%
  ggplot() + 
  geom_segment(aes(x=reorder(state, difference), xend=reorder(state, difference), y=0, yend=difference, color = col), size = 3) + 
  geom_text(aes(label = exercise, x=reorder(state, difference), y = difference + nudge, color = col ), size = rel(3)) + 
  scale_color_identity() +
  labs(x = "", y = "Percentage relative to state average",
       title = "Age-adjusted percentages of adults aged 18–64 who met both aerobic and muscle-strengthening\nguidelines regardless of employment status") +
  coord_flip() +
  theme_light() +
  my_theme

ggsave("bar_chart.png")

# construct word cloud
df_sorted <- df_tidy %>%
  filter(sex == "both", work_status=="total") %>%
  mutate(difference = exercise - both_total_avg$exercise, 
         diff_state = case_when(difference > 0 ~ "#798E87", difference < 0 ~ "#C27D38", TRUE ~ "#CCC591" ),
         abs_diff = abs(difference)) %>%
  arrange(desc(abs_diff)) %>%
  select(state, abs_diff, diff_state)

png("wordcloud.png")
set.seed(3423)
wordcloud(words = df_sorted$state, freq = df_sorted$abs_diff, ordered.colors = TRUE, random.order = FALSE, colors = df_sorted$diff_state, scale = c(3.5,0.4), min.freq = 3)
dev.off()


