
# Tidy Tuesday - Week 16
### Percentage of adults meeting the 2008 US federal guidelines for both aerobic and muscle-strengthening activities by state

My contribution to week 16 of TidyTuesday. I started with bars, which led to segments, which I often do so I decided to mix it up and try a word cloud. 

See https://github.com/rfordatascience/tidytuesday for more information.

#### Wordcloud -- font size relative to amount above/below the state average
![](wordcloud.png)

#### bar chart
![](bar_chart.png)
